# ZMQ connections

Port numbers are passed to application as environment variables. 
 * **PUB_PORT** - Connect **PUBLISHER** type sockets to this port.
 * **SUB_PORT** - Connect **SUBSCRIBER** type sockets to this port.

## Build and run

1. Build: `docker-compose build`
2. Run: `docker-compose up -d`


