#include <cstdio>
#include <vector>
#include <sys/sysinfo.h>
#include <zmq.hpp>
#include <spdlog/spdlog.h>

void usage(const char* name) {
    std::printf("USAGE: \n"
                "%s pub_port sub_port [threads_num]\n"
                "where:\n"
                "  pub_port - port number where publishers connects to\n"
                "  sub_port  - port number where subscribers connects to\n"
                "  threads_num   - optional, number of threads created in zmq\n"
                "                  thread pool. default = num of cpus - 1.\n",
                name);
}

int main(int argc, char* argv[]) {
    std::vector<char*> args(argv, argv + argc);

    if (args.size() < 3 || args.size() > 4) {
        usage(args.front());
        return -1;
    }

    std::string pub_port = std::string(args[1]);
    std::string sub_port = std::string(args[2]);

    int threads_num = get_nprocs() - 1;
    if (args.size() == 4) {
        threads_num = std::stoi(args.back());
    }

    zmq::context_t ctx(threads_num);
    spdlog::info("Context created with {} threads.", threads_num);

    zmq::socket_t pub(ctx, zmq::socket_type::xsub);
    pub.bind("tcp://*:" + pub_port);
    spdlog::info("Publishers socket opened at {} port.", pub_port);

    zmq::socket_t sub(ctx, zmq::socket_type::xpub);
    sub.bind("tcp://*:" + sub_port);
    spdlog::info("Subscribers socket opened at {} port.", sub_port);

    spdlog::info("Starting proxy.");
    zmq::proxy(pub, sub);

    return 0;
}
